# **terminal-calendar**

terminal-calendar is a calendar program desigined to be used on the terminal!

## **Launching**

The program has only been designed for Linux, so no promises that it will work on other platforms.
You must have the rust or rust installed to build and run manually.

### Building
```bash
cargo run
```
### Executing binaries
You can use the compiled [releases](https://codeberg.org/TotalDarkness/terminal-calendar/releases) to launch.\
Simply download it, make it executable, and run it.

```bash
chmod +x /path/to/your/binary
./path/to/your/binary
```

## Usage
The program will begin with a grid of calendars and will place as much as it can fit.\
You are able to increase or decrease the amount shown by changing the size of the terminal.\
You are able to use both the mouse and keyboard to interact with the calendars. It is possible to move around each calendar, as well as within the specific selected calendar.\
You are also able to move forward and backwards in time in large or small amounts of time.\
The files are save and stored in your config-direcotry/terminal-calendar/calendars\
This allows you to set plans or reminders for that day.\
You can see all plans for that month having the month button pressed.

## **Arguments**
|Name|Description|
|---|---|
|-c or --config|Specify the config file to use|
|-s or --setting|Specify the save file to use|

## **Configurations**
The programs provides many ways to configure it. The default configuration would look like this in your config-direcotry/terminial-calendar/config.
```
#lines starting with '#' are ignored
#This allows to make notes if wanted

# This is color settings
bg_color = light blue
calendar_bg_color = white
date_bg_color = black
text_button_bg_color = cyan
date_num_color = white
month_text_color = white
weekday_bg_color = light red
select_bg_date_color = magenta
select_bg_text_button_color = light magenta

# This is key settings
quit = q
edit = enter
up = w
left = a
down = s
right = d
calendar_up = W
calendar_left = A
calendar_down = S
calendar_right = D
go_back_time = left
go_forward_time = right
go_back_calendar = down
go_forward_calendar = up

# This is boolean settings
change_calendar_reset_cursor = true
unselect_change_calendar_cursor = true

# This is number settings
max_threads = 1
```

### **Config Settings**
A configurations starts with the name then its value.
```
setting name = value
```
The spacing or capitlization does not matter.

#### **Color Settings**
|Name|Description|Default|
|---|---|---|
|bg_color|Background color|Light Blue|
|calendar_bg_color|Background color of the calendars|White|
|date_bg_color|Background color of the calendar dates|Black|
|text_button_bg_color|Background color of calendar tops|Cyan|
|date_num_color|Foreground color of date numbers|White|
|month_text_color|Foreground color of calendar tops|White|
|weekday_bg_color|Background color of weekdays|Light Red|
|select_bg_date_color|Background color of a selected date|Magenta|
|select_bg_text_button_color|Foreground color of selected item|Light Magenta|

#### **Color Values**
These values can be used to select a specific color.

##### **Normal Colors**
|Color|Ansi value|
|---|---|
|Black|0|
|Red|1|
|Green|2|
|Yellow|3|
|Blue|4|
|Magenta|5|
|Cyan|6|
|White|7|
|Light Black|8|
|Light Red|9|
|Light Green|10|
|Light Yellow|11|
|Light Blue|12|
|Light Magenta|13|
|Light Cyan|14|
|Light White|15|

##### **RGB Colors**
```
rgb(r, g, b)
# Where r, g, and b are positive integers that are ≤ 5
# From 0 to 5
```
##### **Shades of Gray**
```
gray(value) or grey(value)
# Where r, g, and b are positive integers that are ≤ 24
# From 0 to 24
# The lower the value the darker the shade
```

#### **Key Settings**
|Name|Description|Default|
|---|---|---|
|quit|Quit the program|<kbd>q</kbd>|
|edit|Edit button at cursor|<kbd>ENTER</kbd>|
|edit_config|Edit the config file|<kbd>F2</kbd>|
|up|Move cursor up in a calendar|<kbd>w</kbd>|
|left|Move cursor left in a calendar|<kbd>a</kbd>|
|down|Move cursor down in a calendar|<kbd>s</kbd>|
|right|Move cursor right in a calendar|<kbd>d</kbd>|
|calendar_up|Move up a calendar|<kbd>SHIFT</kbd> + <kbd>w</kbd>|
|calendar_left|Move left a calendar|<kbd>SHIFT</kbd> + <kbd>a</kbd>|
|calendar_down|Move down a calendar|<kbd>SHIFT</kbd> + <kbd>s</kbd>|
|calendar_right|Move right a calendar|<kbd>SHIFT</kbd> + <kbd>d</kbd>|
|go_back_time|Move backwards in time by amount of calendars on screen|<kbd>←</kbd>|
|go_forward_time|Move forward in time by amount of calendars on screen|<kbd>→</kbd>|
|go_back_calendar|Move backwards in time by one calendar|<kbd>↓</kbd>|
|go_forward_calendar|Move forwards in time by one calendar|<kbd>↑</kbd>|

#### **Key Values**
|Value|Notes|
|---|---|
|char|<kbd>char</kbd> char is a character|
|alt(char)|<kbd>ALT</kbd> + <kbd>char</kbd>|
|ctrl(char) or control(char)|<kbd>CTRL</kbd> + <kbd>char</kbd>|
|f(u8) or function(u8)|<kbd>F#</kbd> # is from 1 to 12|
|backspace or back space|<kbd>BACKSPACE</kbd>|
|left|<kbd>←</kbd>|
|right|<kbd>→</kbd>|
|up|<kbd>↑</kbd>|
|down|<kbd>↓</kbd>|
|home|<kbd>HOME</kbd> |
|end|<kbd>END</kbd> |
|pageup or page up|<kbd>PAGEUP</kbd> |
|pagedown or page down|<kbd>PAGEDOWN</kbd> |
|space|<kbd>SPACE</kbd> |
|tab|<kbd>TAB</kbd>|
|backtab or back tab|<kbd>SHIFT</kbd> + <kbd>TAB</kbd>|
|delete|<kbd>DELETE</kbd>|
|insert|<kbd>INSERT</kbd>|
|esc or escape|<kbd>ESC</kbd>|
|ent or enter|<kbd>ENTER</kbd>|
|null|No idea what key this is (null byte)|

#### **Boolean Settings**
|Name|Description|Default|
|---|---|---|
|mouse|Add mouse support|true|
|change_calendar_reset_cursor|On change of calendar reset the previous calendars cursor|true|
|unselect_change_calendar_cursor|Unselect the last curosr on the previous calendar|true|

#### **Boolean Values**
|Boolean Value|Accepted Value|
|---|---|
|true|true, yes, y, t, !false, notfalse, accept, positive, in, :), :3|
|false|false, no, n, f, !true, nottrue, deny, negative, out, :(, :#|

#### **Number Settings**
|Name|Description|Default|
|---|---|---|
|max_threads|The maximum amount of extra threads to use|1|
#### **String Settings**
|Name|Description|Default|
|---|---|---|
|config_file|The path to the file that contains the config options|users config dir/terminal-calendar/config|
|save_file|The path to the file that will save calendar data|users config dir/terminal-calendar/calendars|
## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)