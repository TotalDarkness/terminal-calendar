use crate::{calendar::Calendar, colors::Color, date_data::DateData, position::Position, terminal::Formatter};

#[derive(Clone)]
pub struct Button {
    pub button_data: ButtonType,
    pub offset: ButtonOffset,
    pub bg_color: Color,
    pub fg_color: Color,
}

#[derive(Clone)]
pub enum ButtonType {
    TextButton(String),
    CalanderDate(DateData),
}

impl Button {
    pub fn draw_format(&mut self, calendar: &Calendar) -> Formatter {
        match &self.button_data {
            ButtonType::TextButton(text) => self.draw_text_button(text, calendar),
            ButtonType::CalanderDate(date) => self.draw_calendar_date(date.get_day(), calendar),
        }
    }

    pub fn is_hovered(&self, calendar: &Calendar, position: &Position) -> bool {
        position.get_x() >= self.get_start(calendar).get_x()
        && position.get_y() >= self.get_start(calendar).get_y()
        && position.get_x() <= self.get_end(calendar).get_x()
        && position.get_y() <= self.get_end(calendar).get_y()
    }

    pub fn action(&mut self, calendar: Calendar) -> Option<DateData> {
        let text = match &self.button_data {
            ButtonType::TextButton(_) => get_calendar_text(&calendar),
            ButtonType::CalanderDate(date_data) => date_data.get_text(),
        };

        let text = 
        match edit::edit(&text) {
            Ok(edit) => edit.trim_end().to_owned(),
            Err(_) => text,
        };
        
        if let ButtonType::CalanderDate(date_data) = &mut self.button_data {
            if date_data.set_text(text) {
                return Some(date_data.clone())
            }
        }
        None
    }

    fn get_start(&self, calendar: &Calendar) -> Position {
        self.offset.start_position() + calendar.get_start()
    }

    fn get_end(&self, calendar: &Calendar) -> Position {
        self.offset.end_position() + calendar.get_start()
    }

    fn draw_text_button(&self, text: &String, calendar: &Calendar) -> Formatter {
        let start_position = self.get_start(calendar);
        let end_position = self.get_end(calendar);
        let mut center_x: u16 = (end_position.get_x() + start_position.get_x()) / 2;
        let length: u16 = text.chars().count() as u16 / 2;
        if center_x >= length { center_x -= length; }
        let center_y: u16 = (end_position.get_y() + start_position.get_y()) / 2;
        Formatter::new()
        .create_box(&start_position, &end_position, &self.bg_color)
        .go_to(&Position::new(center_x, center_y))
        .bg_color(&self.bg_color)
        .fg_color(&self.fg_color)
        .text(&text)
    }

    fn draw_calendar_date(&self, day: &u8, calendar: &Calendar) -> Formatter {
        let start_position =  self.offset.start_position() + calendar.get_start();
        Formatter::new()
        .go_to(&start_position)
        .bg_color(&self.bg_color)
        .fg_color(&self.fg_color)
        .text(&format!("{:#2}", day))
    }
}

fn get_calendar_text(calendar: &Calendar) -> String {
    let mut text = String::new();
    for button in &calendar.buttons {
        if let ButtonType::CalanderDate(date_data) = &button.button_data {
            if !date_data.get_text().is_empty() {
                text += &date_data.to_string();
            }
        }
    }
    text.trim_end().to_owned()
}

// This structure holds bit shifted values of x and y
// Some basic testing shows that when in release mode this can help save memory
#[derive(Clone)]
pub struct ButtonOffset {
    start: u16,
    end: u16,
}

impl ButtonOffset {
    pub fn new(start_x: u8, start_y: u8, end_x: u8, end_y: u8) -> Self {
        ButtonOffset { 
            start: ButtonOffset::set_offset(start_x, start_y),
            end: ButtonOffset::set_offset(end_x, end_y),
        }   
    }

    fn set_offset(x: u8, y: u8) -> u16 {
        let mut offset: u16 = 0;
        offset |= (x as u16) << 8;
        offset | y as u16
    }

    fn unpack(offset: &u16) -> (u16, u16) {
        (offset >> 8, offset << 8 >> 8)
    }

    fn start_position(&self) -> Position {
        let (x, y) = ButtonOffset::unpack(&self.start);
        Position::new(x, y)
    }

    fn end_position(&self) -> Position {
        let (x, y) = ButtonOffset::unpack(&self.end);
        Position::new(x, y)
    }
}

pub struct TextBox {
    text: String,
    pub position: Position,
    color: Color,
}

impl TextBox {
    pub fn new(text: String, position: Position, color: Color) -> Self {
        TextBox { text, position, color }
    }
    pub fn draw_format(&self) -> Formatter {
        Formatter::new().go_to(&self.position).bg_color(&self.color).text(&self.text)
    }
}