use chrono::{Date, Datelike, Local, Weekday};

use crate::{button::{Button, ButtonOffset, ButtonType, TextBox}, colors::Color, config::Config, date_data::DateData, position::{Direction, Position}, terminal::Formatter};

static mut WEEKDAYS: Option<TextBox> = None;

#[derive(Clone)]
pub struct Calendar {
    start: Position,
    end: Position,
    pub buttons: Vec<Button>,
    pub cursor: usize,
    bg_color: Color,
}

impl Calendar {
    pub fn new(start_date: Date<Local>, start: Position, data: Vec<DateData>, config: &Config) -> Self {
        let end = Position::new(start.get_x() + 21, start.get_y() + 12);
        let mut calendar = Calendar {
            start,
            end,
            buttons: Vec::new(),
            cursor: 0,
            bg_color: config.calendar_bg_color,
        };
        calendar.setup(&start_date, data, config);
        setup_weekdays(start_date, config.weekday_bg_color);
        calendar
    }

    pub fn dummy() -> Self {
        Calendar {
            start: Position::new_origin(),
            end: Position::new_origin(),
            buttons: Vec::new(),
            cursor: 0,
            bg_color: Color::new(0),
        }
    }

    fn setup(&mut self, start_date: &Date<Local>, mut data: Vec<DateData>, config: &Config) {
        let mut date = start_date.clone();
        // Make title bar button
        let button = Button {
            button_data: ButtonType::TextButton(date.format("%B %Y").to_string()),
            offset: ButtonOffset::new(0, 0, 21, 0),
            bg_color: config.text_button_bg_color,
            fg_color: config.month_text_color,
        };
        self.buttons.push(button);
        let mut position = self.start.clone();
        position.set(
            position.get_x() + 1 + 3 * (date.weekday().number_from_sunday() as u16 - 1),
            position.get_y() + 2,
        );

        // Count days in a month
        let days = {
            let mut date = date.clone();
            let mut days = 0;
            while start_date.month() == date.month() {
                days += 1;
                date = date.succ();
            }
            days
        };

        let mut offset_x: u8 = 1 + 3 * (date.weekday().number_from_sunday() as u8 - 1);
        let mut offset_y: u8 = 2;

        // Add days in a month
        for _ in 1..=days {
            let date_data = {
                let mut index: usize = data.len();
                for (i, date_data) in data.iter().enumerate() {
                    if *date_data == date {
                        index = i;
                        break;
                    }
                }
                if index < data.len() {
                    ButtonType::CalanderDate(data.swap_remove(index))
                } else {
                    ButtonType::CalanderDate(DateData::new(&date))
                }
            };
            
            let button = Button {
                button_data: date_data,
                offset: ButtonOffset::new(offset_x, offset_y, offset_x + 1, offset_y),
                bg_color: config.date_bg_color,
                fg_color: config.date_num_color,
            };
            self.buttons.push(button);
            date = date.succ();
            let days_from_max: u8 = date.weekday().num_days_from_sunday() as u8;
            offset_x = 1 + 3 * days_from_max;
            offset_y += if days_from_max == 0 { 2 } else { 0 };
        }
    }

    pub fn move_cursor(&mut self, config: &Config, direction: Direction) -> Formatter {
        let index_to = match direction {
            Direction::Up => 
                if self.cursor <= 7 { 0 } else { self.cursor - 7 },
            Direction::Down => 
                if self.cursor + 7 >= self.buttons.len() { self.buttons.len() - 1 } else { self.cursor + 7 },
            Direction::Left => 
                if self.cursor == 0 { 0 } else { self.cursor - 1 },
            Direction::Right =>
                if self.cursor + 1 >= self.buttons.len() { self.buttons.len() - 1 } else { self.cursor + 1 },
        };
        self.select_button(config, index_to)
    }

    pub fn select_button(&mut self, config: &Config, index_to: usize) -> Formatter {
        let mut format = Formatter::new();
        if index_to >= self.buttons.len() { return format; }
        if self.cursor != index_to { format += &self.unselect_button(config); }
        let calendar = &self.clone();
        let button = self.buttons.get_mut(index_to);
        match button {
            Some(button) => {
                button.bg_color = match button.button_data {
                    ButtonType::TextButton(_) => config.select_bg_text_button_color,
                    ButtonType::CalanderDate(_) => config.select_bg_date_color,
                };
                format += &button.draw_format(calendar);
                self.cursor = index_to;
            }
            None => (),
        }
        format
    }

    pub fn unselect_button(&mut self, config: &Config) -> Formatter {
        let mut format = Formatter::new();
        let calendar = &self.clone();
        match self.buttons.get_mut(self.cursor) {
            Some(button) => {
                button.bg_color = match button.button_data {
                    ButtonType::TextButton(_) => config.text_button_bg_color,
                    ButtonType::CalanderDate(_) => config.date_bg_color,
                };
                format += &button.draw_format(calendar);
            }
            None => (),
        }
        format
    }

    pub fn get_start_date(&self) -> Date<Local> {
        for button in &self.buttons {
            if let ButtonType::CalanderDate(date_date) = &button.button_data {
                return date_date.get_date();
            }
        }
        return Local::today();
    }

    pub fn draw_format(&mut self) -> Formatter {
        let mut format = Formatter::new().create_box(&self.start, &self.end, &self.bg_color);
        let calendar = &self.clone();
        for button in self.buttons.iter_mut() {
            format += &button.draw_format(calendar);
        }
        unsafe {
            match &mut WEEKDAYS {
                Some(textbox) => {
                    textbox.position.set(self.start.get_x(), self.start.get_y() + 1);
                    format += &textbox.draw_format()
                },
                None => (),
            }
        }
        format
    }

    pub fn get_start(&self) -> &Position {
        &self.start
    }

    pub fn get_end(&self) -> &Position {
        &self.end
    }

    pub fn is_hovered(&self, position: &Position) -> bool {
        position.get_x() >= self.get_start().get_x() && position.get_y() >= self.get_start().get_y()
        && position.get_x() <= self.get_end().get_x() && position.get_y() <= self.get_end().get_y()
    }
}

fn get_weekdays(mut date: Date<Local>) -> String {
    while date.weekday() != Weekday::Sun { date = date.succ(); }
    let mut text = String::new();
    for _ in 1..=7 {
        text.push_str(date.format("%a").to_string().as_str());
        date = date.succ();
    }
    text.push(' ');
    text
}

fn setup_weekdays(date: Date<Local>, color: Color) {
    unsafe {
        if let None = WEEKDAYS {
            let weekdays = TextBox::new(get_weekdays(date), Position::new_center(), color);
            WEEKDAYS = Some(weekdays);
        }
    }
}