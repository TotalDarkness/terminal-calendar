use termion::color::AnsiValue;

#[derive(Clone, Copy)]
pub struct Color(AnsiValue);

impl Color {
    pub fn new(value: u8) -> Self {
        Color(AnsiValue(value))
    }

    pub fn from_ansi(color: AnsiValue) -> Self {
        Color(color)
    }

    pub fn get_color(&self) -> &AnsiValue {
        &self.0
    }
}