use std::{fs::{File, OpenOptions}, io::{BufRead, BufReader}, sync::{Arc, Mutex}, thread};

use directories::ProjectDirs;
use termion::{color::AnsiValue, event::Key};

use crate::colors::Color;

/*
Ansi value for color
Black, 0
Red, 1
Green, 2
Yellow, 3
Blue, 4
Magenta, 5
Cyan, 6
White, 7
LightBlack, 8
LightRed, 9
LightGreen, 10
LightYellow, 11
LightBlue, 12
LightMagenta, 13
LightCyan, 14
LightWhite, 15
*/

#[derive(Clone)]
pub struct Config {
    pub bg_color: Color,
    pub calendar_bg_color: Color,
    pub date_bg_color: Color,
    pub text_button_bg_color: Color,
    pub date_num_color: Color,
    pub month_text_color: Color,
    pub weekday_bg_color: Color,
    // TODO make weekday_text_color
    pub select_bg_date_color: Color,
    // TODO add select_bg_month_color
    pub select_bg_text_button_color: Color,
    // TODO make above settings two for date and months
    pub quit: Key,
    pub edit: Key,
    pub edit_config: Key,
    pub up: Key,
    pub left: Key,
    pub down: Key,
    pub right: Key,
    pub calendar_up: Key,
    pub calendar_left: Key,
    pub calendar_right: Key,
    pub calendar_down: Key,
    pub go_back_time: Key,
    pub go_forward_time: Key,
    pub go_back_calendar: Key,
    pub go_forward_calendar: Key,
    pub mouse: bool,
    pub change_calendar_reset_cursor: bool,
    pub unselect_change_calendar_cursor: bool,
    pub max_threads: usize,
    pub config_file: String,
    pub save_file: String,
}

impl Config {
    pub fn get_config() -> Self {
        let config = Config::get_default_config();
        let file = match OpenOptions::new()
        .read(true).write(true).create(true).open(&config.config_file) {
            Ok(file) => file,
            Err(_) => return config,
        };
        let reader = BufReader::new(file);
        let mut handles = Vec::with_capacity(10);
        let mutex = Arc::new(Mutex::new(reader.lines()));
        let threads = config.max_threads;
        let config_mutex = Arc::new(Mutex::new(config));
        for _ in 0..threads {
            let mutex = mutex.clone();
            let config_mutex = config_mutex.clone();
            let handle = thread::spawn( move || {
                loop {
                    let line;
                    { 
                        match mutex.lock().unwrap().next() {
                            Some(value) => line = value.unwrap(),
                            None => break,
                        }
                    }
                    let line = line.trim().to_lowercase();
                    if line.starts_with('#') { continue }
                    let split_index;
                    match line.find('=') {
                        Some(index) => split_index = index,
                        None => continue,
                    }
                    let (config_var, value) = line.split_at(split_index);
                    let value = value.replace(" ", "").replace("=", "");
                    let config_var = config_var.trim();
                    let value = value.trim();
                    {
                        let mut config = config_mutex.lock().unwrap();
                        if config.match_boolean(config_var, value)
                        || config.match_number(config_var, value)
                        || config.match_string(config_var, value)
                        || config.match_colors(config_var, value)
                        || config.match_key(config_var, value) 
                        { continue }
                    }
                }
            });
            handles.push(handle);
        }

        for handle in handles {
            handle.join().unwrap();
        }

        if let Ok(lock) = Arc::try_unwrap(config_mutex) {
            lock.into_inner().unwrap()
        } else { Config::get_default_config() }
    }

    fn get_default_config() -> Self {
        let mut config_file = String::new(); 
        let mut save_file = String::new();
        let mut max_threads = 1;
        if let Some(dir) = ProjectDirs::from("net", "totaldarkness",  "terminal-calendar") {
            let config = dir.config_dir();
            if !config.exists() {
                match std::fs::create_dir_all(config) {
                    Ok(_) => (),
                    Err(_) => (),
                }
            }
            config_file = get_config_path(dir.config_dir().join("config").to_str().unwrap().to_string()); 
            save_file = get_save_path(&config_file, dir.config_dir().join("calendars").to_str().unwrap().to_string());
            max_threads = get_max_threads(&config_file);
        }
        Config {
            bg_color: Color::new(12),
            calendar_bg_color: Color::new(7),
            date_bg_color: Color::new(0),
            text_button_bg_color: Color::new(6),
            date_num_color: Color::new(7),
            month_text_color: Color::new(7),
            weekday_bg_color: Color::new(9),
            select_bg_date_color: Color::new(5),
            select_bg_text_button_color: Color::new(13),
            quit: Key::Char('q'),
            edit: Key::Char('\n'),
            edit_config: Key::F(2),
            up: Key::Char('w'),
            left: Key::Char('a'),
            down: Key::Char('s'),
            right: Key::Char('d'),
            calendar_up: Key::Char('W'),
            calendar_left: Key::Char('A'),
            calendar_down: Key::Char('S'),
            calendar_right: Key::Char('D'),
            go_back_time: Key::Left,
            go_forward_time: Key::Right,
            go_back_calendar: Key::Down,
            go_forward_calendar: Key::Up,
            mouse: true,
            change_calendar_reset_cursor: true,
            unselect_change_calendar_cursor: true,
            max_threads,
            config_file,
            save_file,
        }
    }

    fn match_boolean(&mut self, config_var: &str, value: &str) -> bool {
        let config = self;
        if let Some(value) = parse_boolean(value) {
            match config_var {
                "mouse" => config.mouse = value,
                "change_calendar_reset_cursor" => config.change_calendar_reset_cursor = value,
                "unselect_change_calendar_cursor" => config.unselect_change_calendar_cursor = value,
                _ => return false,
            }
            return true;
        }
        false
    }

    fn match_number(&mut self, config_var: &str, value: &str) -> bool {
        let config = self;
        if let Ok(value) = value.parse::<usize>() {
            match config_var {
                "max_threads" => if value != 0 { config.max_threads = value },
                _ => return false,
            }
            return true;
        }
        false
    }

    fn match_string(&mut self, config_var: &str, value: &str) -> bool {
        let config = self;
        match config_var {
            "config_file" => config.config_file = value.to_owned(),
            "save_file" => config.save_file = value.to_owned(),
            _ => return false,
        }
        true
    }

    fn match_colors(&mut self, config_var: &str, value: &str) -> bool {
        let config = self;
        if let Some(value) = parse_color(value) {
            match config_var {
                "bg_color" => config.bg_color = value,
                "calendar_bg_color" => config.calendar_bg_color = value,
                "date_bg_color" => config.date_bg_color = value,
                "text_button_bg_color" => config.text_button_bg_color = value,
                "date_num_color" => config.date_num_color = value,
                "month_text_color" => config.month_text_color = value,
                "weekday_bg_color" => config.weekday_bg_color = value,
                "select_bg_date_color" => config.select_bg_date_color = value,
                "select_bg_text_button_color" => config.select_bg_text_button_color = value,
                _ => return false,
            }
            return true
        }
        false
    }

    fn match_key(&mut self, config_var: &str, value: &str) -> bool {
        let config = self;
        if let Some(key) = parse_key(value) {
            match config_var {
                "quit" => config.quit = key,
                "edit" => config.edit = key,
                "edit_config" => config.edit_config = key,
                "up" => config.up = key,
                "left" => config.left = key,
                "down" => config.down = key,
                "right" => config.right = key,
                "calendar_up" => config.calendar_up = key,
                "calendar_left" => config.calendar_left = key,
                "calendar_right" => config.calendar_right = key,
                "calendar_down" => config.calendar_down = key,
                "go_back_time" => config.go_back_time = key,
                "go_forward_time" => config.go_forward_time = key,
                "go_back_calendar" => config.go_back_calendar = key,
                "go_forward_calendar" => config.go_forward_calendar = key,
                _ => return false,
            }
            return true
        }
        false
    }

    pub fn get_file(&self) -> std::io::Result<File> {
        OpenOptions::new().read(true).write(true).create(true).open(&self.save_file)
    }
}

fn parse_boolean(color_string: &str) -> Option<bool> {
    if let Ok(bool) = color_string.parse::<bool>() {
        return Some(bool);
    } else {
        match color_string {
            "no" | "n" | "f" | "!true" | "nottrue" | "deny" | "negative" | "out" | ":(" | ":#" =>
            Some(false),
            "yes" | "y" | "t" | "!false" | "notfalse" | "accept" | "positive" | "in" | ":)" | ":3" =>
            Some(true),
            _ => None,
        }
    }
}

fn parse_color(mut color_string: &str) -> Option<Color> {
    if color_string.contains("gray") || color_string.contains("grey") { return parse_gray_shade(color_string); }
    else if color_string.contains("rgb") { return  parse_rgb_color(color_string); }
    if color_string.starts_with("high-intensity") {
        color_string = color_string.split_at("high-intensity".len()).1;
    }
    match color_string {
        "black" | "0" => Some(Color::new(0)),
        "red" | "1" => Some(Color::new(1)),
        "green" | "2" => Some(Color::new(2)),
        "yellow" | "3" => Some(Color::new(3)),
        "blue" | "4" => Some(Color::new(4)),
        "magenta" | "5" => Some(Color::new(5)),
        "cyan" | "6" => Some(Color::new(6)),
        "white" | "7" => Some(Color::new(7)),
        "lightblack" | "8" => Some(Color::new(8)),
        "lightred" | "9" => Some(Color::new(9)),
        "lightgreen" | "10" => Some(Color::new(10)),
        "lightyellow" | "11" => Some(Color::new(11)),
        "lightblue" | "12" => Some(Color::new(12)),
        "lightmagenta" | "13" => Some(Color::new(13)),
        "lightcyan" | "14" => Some(Color::new(14)),
        "lightwhite" | "15" => Some(Color::new(15)),
        _ => None,
    }
}

fn parse_rgb_color(color_string: &str) -> Option<Color> {
    let color_string = color_string
        .replace("rgb", "")
        .replace("(", "")
        .replace(")", "");
    let args: Vec<&str> = color_string.split(",").collect();
    if args.len() != 3 { return  None; }
    let mut rgb: [u8; 3] = [0; 3];
    for (index, arg) in args.iter().enumerate() {
        let value = 
        match arg.parse::<u8>() {
            Ok(u8) => u8,
            Err(_) => return None,
        };
        if value > 5 { return None; }
        rgb[index] = value;
    }
    Some(Color::from_ansi(AnsiValue::rgb(rgb[0], rgb[1], rgb[2])))
}

fn parse_gray_shade(color_string: &str) -> Option<Color> {
    let color_string = color_string
        .replace("gray", "")
        .replace("grey", "")
        .replace("(", "")
        .replace(")", "");
    
    match color_string.parse::<u8>() {
        Ok(u8) => {
            if u8 > 24 { return None; }
            if u8 == 0 { return Some(Color::new(0)) } // Return black
            let u8 = u8 - 1;
            Some(Color::from_ansi(AnsiValue::grayscale(u8)))
        },
        Err(_) => None,
    }
}

fn parse_key(key_string: &str) -> Option<Key> {
    if let Ok(char) = key_string.parse::<char>(){
        return Some(Key::Char(char));
    } else if key_string.contains('(') && key_string.contains(')') {
        let index = key_string.find("(").unwrap();
        let (key, value) = key_string.split_at(index);
        let value = value.replace('(', "").replace(')', "");
        match key {
            "alt" => 
                if let Ok(char) = value.parse::<char>() {
                    return Some(Key::Alt(char));
                },
            "ctrl" | "control" => 
                if let Ok(char) = value.parse::<char>() {
                    return Some(Key::Ctrl(char));
                },
            "f" | "function" =>
                if let Ok(u8) = value.parse::<u8>() {
                    if u8 >= 1 && u8 <= 12 { return Some(Key::F(u8)); }
                },
            _ => (),
        }
    } else {
        return match key_string {
            "backspace" | "back space" => Some(Key::Backspace),
            "left" => Some(Key::Left),
            "right" => Some(Key::Right),
            "up" => Some(Key::Up),
            "down" => Some(Key::Down),
            "home" => Some(Key::Home),
            "end" => Some(Key::End),
            "pageup" | "page up" => Some(Key::PageUp),
            "pagedown" | "page down" => Some(Key::PageDown),
            "space" => Some(Key::Char(' ')),
            "tab" => Some(Key::Char('\t')),
            "backtab" | "back tab" => Some(Key::BackTab),
            "delete" => Some(Key::Delete),
            "insert" => Some(Key::Insert),
            "esc" | "escape" => Some(Key::Esc),
            "ent" | "enter" => Some(Key::Char('\n')),
            "null" => Some(Key::Null), // What is the null byte?
            _ => None,
        }
    }
    None
}

fn get_config_path(path: String) -> String {
    let mut args = std::env::args().into_iter();
    loop {
        match args.next() {
            Some(arg) => {
                if arg.starts_with("--config") || arg.starts_with("-c") {
                    return args.next().unwrap_or(path);
                }
            },
            None => break,
        }
    }
    find_config_setting(&path, "config_file".to_owned()).unwrap_or(path)
}

fn get_save_path(path: &String, default: String) -> String {
    let mut args = std::env::args().into_iter();
    loop {
        match args.next() {
            Some(arg) => {
                if arg.starts_with("--save") || arg.starts_with("-s") {
                    return args.next().unwrap_or(default);
                }
            },
            None => break,
        }
    }
    find_config_setting(&path, "save_file".to_owned()).unwrap_or(default)
}

fn get_max_threads(path: &String) -> usize {
    match find_config_setting(path, "max_threads".to_owned()) {
        Ok(value) => {
            match value.parse::<usize>() {
                Ok(value) => value,
                Err(_) => 1,
            }
        },
        Err(_) => 1,
    }
}

fn find_config_setting(path: &String, setting: String) -> Result<String, ()> {
    match File::open(path) {
        Ok(file) => {
            for line in BufReader::new(file).lines() {
                if let Ok(line) = line {
                    let line = line.trim().to_lowercase();
                    if line.starts_with('#') { continue }
                    let split_index;
                    match line.find('=') {
                        Some(index) => split_index = index,
                        None => continue,
                    }
                    let (config_var, value) = line.split_at(split_index);
                    let value = value.replace(" ", "").replace("=", "");
                    if config_var.trim() == setting {
                        return Ok(value.trim().to_owned())
                    }
                }
            }
        },
        Err(_) => (),
    }
    Err(())
}