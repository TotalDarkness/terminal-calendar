use chrono::{Date, Datelike, Local, TimeZone};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct DateData {
    year: i32,
    month: u8,
    day: u8,
    text: String,
}

impl ToString for DateData {
    fn to_string(&self) -> String {
        format!("[{}-{}]\n{}\n", self.day, self.get_date().format("%B-%Y").to_string(), self.text)
    }
}

impl PartialEq for DateData {
    fn eq(&self, other: &Self) -> bool {
        self.year == other.year && self.month == other.month && self.day == other.day
    }
}

impl <T: TimeZone> PartialEq<Date<T>> for DateData {
    fn eq(&self, other: &Date<T>) -> bool {
        self.year == other.year() && self.month == other.month() as u8 && self.day == other.day() as u8
    }
}

impl DateData {
    pub fn new(date: &Date<Local>) -> Self {
        DateData {
            year: date.year(),
            month: date.month() as u8,
            day: date.day() as u8,
            text: String::new(),
        }
    }
    
    pub fn set_text(&mut self, text: String) -> bool {
        if self.text != text {
            self.text = text;
            true
        } else { false }
    }

    pub fn get_date(&self) -> Date<Local> {
        Local.ymd(self.year, self.month as u32, self.day as u32)
    }

    pub fn get_day(&self) -> &u8 {
        &self.day
    }

    pub fn get_month(&self) -> &u8 {
        &self.month
    }

    pub fn get_year(&self) -> &i32 {
        &self.year
    }

    pub fn get_text(&self) -> String {
        self.text.clone()
    }
}