mod button;
mod calendar;
mod colors;
mod config;
mod date_data;
mod position;
mod saving;
mod terminal;
mod tui;

use crate::tui::Tui;

fn main() {
    Tui::new().start();
}