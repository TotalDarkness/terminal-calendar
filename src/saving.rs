use std::{fs::{File, OpenOptions}, io::{BufReader, Error}};
use crate::date_data::DateData;

pub struct Save {
    file_path: String,
}

impl Save {
    pub fn new(file_path: String) -> Self {
        let save = Save { file_path };
        match save.get_old_data() {
            Ok(data) => 
            if data.is_empty() {
                // Create initial file with empty vector
                save.save_vec_data(&data);
            },
            Err(_) => (),
        }
        save
    }

    pub fn save_date_data(&self, date_data: DateData) {
        // TODO log savings?
        if let Ok(data) = self.get_old_data() {
            let file = match self.get_file() {
                Ok(file) => file,
                Err(_) => panic!("No file"),
            };
            let data = Save::add_data(date_data, data);
            match bincode::serialize_into(file, &data) {
                Ok(_) => (),
                Err(_) => panic!("Could not serialize"),
            }
        }
    }

    pub fn save_vec_data(&self, data: &Vec<DateData>) {
        let file = match self.get_file() {
            Ok(file) => file,
            Err(_) => panic!("No file"),
        };
        match bincode::serialize_into(file, &data) {
            Ok(_) => (),
            Err(_) => panic!("Could not serialize"),
        }
    }

    pub fn get_old_data(&self) -> Result<Vec<DateData>, Error> {
        let file = match self.get_file() {
            Ok(file) => file,
            Err(error) => return Err(error),
        };
        let bufreader = BufReader::new(file);
        let date_data_vec: Vec<DateData> = match bincode::deserialize_from(bufreader) {
            Ok(vec) => vec,
            Err(_) => Vec::new(), // Likely no data in file
        };
        Ok(date_data_vec)
    }

    fn add_data(date_data: DateData, mut data: Vec<DateData>) -> Vec<DateData> {
        for d in &mut data {
            if date_data == *d {
                *d = date_data;
                return data;
            }
        }
        data.push(date_data);
        data
    }

    fn get_file(&self) -> std::io::Result<File> {
        OpenOptions::new().read(true).write(true).create(true).open(&self.file_path)
    }
}