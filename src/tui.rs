use std::{sync::{Arc, Mutex, mpsc::{Receiver, Sender, channel}}, thread};

use chrono::{Date, Datelike, Local};
use termion::event::{Event, Key, MouseButton, MouseEvent};

use crate::{button::ButtonType, calendar::Calendar, config::Config, date_data::DateData, position::{self, Direction, Position}, saving::Save, terminal::{self, Formatter, Terminal}};

pub struct Tui {
    bounds: Position,
    config: Config,
    terminal: Terminal,
    calendars: Vec<Calendar>,
    tx: Option<Sender<bool>>,
    save: Option<Save>,
}

impl Tui {
    pub fn new() -> Self {
        Tui {
            bounds: position::get_boundaries(),
            config: Config::get_config(),
            terminal: Terminal::new_raw(),
            calendars: Vec::new(),
            tx: None,
            save: None,
        }
    }

    pub fn start(&mut self) {
        self.terminal.begin();
        self.tui_loop();
        self.end();
    }

    fn end(&mut self) {
        self.terminal.exit();
        if let Some(save) = &self.save {
            if let Ok(data) = save.get_old_data() {
                let mut data: Vec<DateData> = data;
                for calendar in &self.calendars {
                    for button in &calendar.buttons {
                        if let ButtonType::CalanderDate(date_data) = &button.button_data {
                            if !date_data.get_text().is_empty() && !data.contains(date_data) {
                                data.push(date_data.clone());
                            }
                        }
                    }
                }
                save.save_vec_data(&data);
            }   
        }        
    }

    fn init(&mut self, date: Date<Local>) {
        self.create_calendars(date);
        self.terminal.write_format(self.draw_calendars());
    }

    fn tui_loop(&mut self) {
        let (tx, rx) = channel();
        let (tx1, rx1) = channel();
        self.tx = Some(tx1);
        thread::spawn(move || {
            let mut events = terminal::get_events();
            loop {
                // What I do here is wait to see if an input is desired
                if !rx1.recv().unwrap() {
                    loop { if rx1.recv().unwrap() { break; } } // Wait until it is unpaused to try inputs again
                } else { tx.send(events.next().unwrap().unwrap()).unwrap(); }
            }
        });
        self.save = match self.config.get_file() {
            Ok(_) => Some(Save::new(self.config.save_file.clone())),
            Err(_) => None,
        };
        self.init(Local::today().with_day(1).unwrap());
        if self.config.mouse {self.terminal.mouse_terminal();}
        let mut quit: bool = false;
        let mut calendar_index: usize = 0; 
        self.tx.as_ref().unwrap().send(true).unwrap(); // Request an input
        while !quit {
            self.handle_event(&mut calendar_index, &mut quit, &rx);
            if self.bounds != position::get_boundaries() {
                calendar_index = 0;
                self.reset(Local::today().with_day(1).unwrap());
            }
        }
    }

    fn handle_event(&mut self, index: &mut usize, quit: &mut bool, rx: &Receiver<Event>) {
        if let Ok(event) = rx.try_recv() {
            match event {
                Event::Key(key) => self.handle_key(key, index, quit),
                Event::Mouse(mouse) => self.handle_mouse(mouse, index),
                Event::Unsupported(_) => (),
            };
            self.tx.as_ref().unwrap().send(true).unwrap(); // Request to look for another input
        }
    }

    fn handle_key(&mut self, key: Key, index: &mut usize, quit: &mut bool) {
        let config = &self.config;
        if key == config.quit {
            *quit = true;
        } else if key == config.edit_config {
            self.edit_begin();
            match edit::edit_file(&self.config.config_file) {
                Ok(_) => {
                    self.config = Config::get_config();
                    self.save = Some(Save::new(self.config.save_file.clone()));
                },
                Err(_) => (),
            }
            self.edit_end();
            self.terminal.write_format(self.draw_calendars());
        } else if key == config.edit {
           self.edit(index);
        } else if key == config.go_back_time {
            self.reset(self.time_travel(Direction::Left));
        } else if key == config.go_forward_time {
            self.reset(self.time_travel(Direction::Right));
        } else if key == config.go_back_calendar {
            self.reset(self.time_travel(Direction::Down));
        } else if key == config.go_forward_calendar {
            self.reset(self.time_travel(Direction::Up));
        } else {
            let format =
            if key == config.left {
                self.calendars.get_mut(*index).unwrap()
                .move_cursor(&config, Direction::Left)
            } else if key == config.right {
                self.calendars.get_mut(*index).unwrap()
                .move_cursor(&config, Direction::Right)
            } else if key == config.up {
                self.calendars.get_mut(*index).unwrap()
                .move_cursor(&config, Direction::Up)
            } else if key == config.down {
                self.calendars.get_mut(*index).unwrap()
                .move_cursor(&config, Direction::Down)
            } else if key == config.calendar_left {
                self.move_calendar(index, Direction::Left)
            } else if key == config.calendar_right {
                self.move_calendar(index, Direction::Right)
            } else if key == config.calendar_up {
                self.move_calendar(index, Direction::Up)
            } else if key == config.calendar_down {
                self.move_calendar(index, Direction::Down)
            } else { return };
            self.terminal.write_format(format);
        }
    }

    fn edit(&mut self, index: &usize) {
        self.edit_begin();
        let calendar = 
        match self.calendars.get_mut(*index) {
            Some(calendar) => calendar,
            None => return,
        };

        let temp_calendar = calendar.clone();

        let button = 
        match calendar.buttons.get_mut(calendar.cursor) {
            Some(button) => button,
            None => return,
        };
        if let Some(date_data) = button.action(temp_calendar) {
            self.save(date_data);
        }
        self.edit_end();
        let draw = self.draw_calendars();
        self.terminal.write_format(draw);
    }

    fn edit_begin(&mut self) {
        self.tx.as_ref().unwrap().send(false).unwrap(); // Pause key requests
        self.terminal.reset();
        self.terminal.exit();
        self.terminal = Terminal::new(); // Drop the raw mode and mouse terminal
    }

    fn edit_end(&mut self) {
        self.terminal = Terminal::new_raw();
        self.terminal.begin();
        if self.config.mouse {self.terminal.mouse_terminal();}
        self.tx.as_ref().unwrap().send(true).unwrap(); // Unpause key requests
    }

    fn save(&self, date_data: DateData) {
        if let Some(save) = &self.save {
            save.save_date_data(date_data);
        }
    }

    fn time_travel(&self, direction: Direction) -> Date<Local> {
        if self.calendars.is_empty() { return Local::today().with_day(1).unwrap() }
        let date = self.calendars.first().unwrap().get_start_date();
        let other_date = self.calendars.last().unwrap().get_start_date();
        let result = 
        match direction {
            Direction::Left => {
                let other_date = other_date + chrono::Duration::days(5); // Skip a few days into that month.
                date + date.signed_duration_since(other_date)
            }, 
            Direction::Right => {
                let other_date = other_date + chrono::Duration::days(32); // Skip a month and a bit
                date + other_date.signed_duration_since(date)
            },
            Direction::Up => date + chrono::Duration::days(32),
            Direction::Down => date - chrono::Duration::days(5),
        }.with_day(1);

        match result {
            Some(value) => value,
            None => {
                if matches!(direction, Direction::Left) || matches!(direction, Direction::Down) {
                    chrono::MIN_DATE.with_timezone(&Local)
                } else {
                    chrono::MAX_DATE.with_timezone(&Local)
                }
            },
        }
    }

    fn handle_mouse(&mut self, mouse: MouseEvent, index: &mut usize) {
        if let MouseEvent::Press(mouse, x, y) = mouse {
            if mouse != MouseButton::Left && mouse != MouseButton::Right { return; }
            let mut calendar_change = false;
            let mut future_index = self.calendars.len() + 1;
            let mouse_pos = Position::new(x, y);
            let mut format = Formatter::new();
            for (calendar_index, calendar) in self.calendars.iter_mut().enumerate() {
                if !calendar.is_hovered(&mouse_pos) { continue }
                for (i, button) in calendar.buttons.iter().enumerate() {
                    if !button.is_hovered(&calendar, &mouse_pos) { continue }
                    if *index != calendar_index {
                        calendar_change = true;
                        future_index = calendar_index;
                    }
                    format += &calendar.select_button(&mut self.config, i);
                    break;
                }
            }

            if calendar_change { 
                let last_calendar = self.calendars.get_mut(*index).unwrap();
                if self.config.unselect_change_calendar_cursor || self.config.change_calendar_reset_cursor {
                    format += &last_calendar.unselect_button(&mut self.config);
                }
                if self.config.change_calendar_reset_cursor { last_calendar.cursor = 0; }  
                *index = future_index;  
            }
            
            if matches!(mouse, MouseButton::Right) {
                self.edit(index);
            } else { self.terminal.write_format(format); }
        }
    }

    fn draw_background(&self) -> Formatter {
        Formatter::new().
        create_box(&Position::new_origin(), &Position::new(self.bounds.get_x(), self.bounds.get_y()), &self.config.bg_color)
    }

    pub fn create_calendars(&mut self, date: Date<Local>) {
        let columns = get_columns();
        let rows = get_rows();
        let threads = self.config.max_threads;
        let threads = 
        if rows > threads || columns > threads { threads } 
        else if rows >= columns { rows }
        else { columns };
        // Put all this here because they all relate and need to be in sync
        let mutex = Arc::new(Mutex::new((date, Position::new_origin(), 0)));
        let max = rows * columns;
        let mut vec = vec![Calendar::dummy(); max]; //fill up space
        let mut old_data: Vec<DateData> = Vec::new();
        if let Some(save) = &self.save {
            if let Ok(data) = save.get_old_data() {
                old_data = data;
            }
        }
        let old_data = Arc::new(Mutex::new(old_data));
        let (tx, rx) = channel();
        let handles: Vec<_> = (0..threads).map(|_| {
            let config = self.config.clone();
            let mutex = mutex.clone();
            let old_data = old_data.clone();
            let tx = tx.clone();
            thread::spawn(move || {
                loop {
                    let date;
                    let position;
                    let index;
                    { // Put in a new scope to force the lock drop and unlock for other threads
                        let mut lock  = mutex.lock().unwrap();
                        if lock.2 >= max { break; }
                        date = lock.0.clone();
                        position = lock.1.clone();
                        index = lock.2.clone();
                        lock.0 = (date + chrono::Duration::days(32)).with_day(1).unwrap();
                        lock.1.set_x(position.get_x() + 24);
                        lock.2 += 1;
                        if lock.2 % get_columns() == 0 { lock.1.set(1, position.get_y() + 14); }
                    }
                    let mut data: Vec<DateData> = Vec::new();
                    {
                        let mut lock = old_data.lock().unwrap();
                        lock.retain(|date_data| {
                            if *date_data.get_year() == date.year() && *date_data.get_month() == date.month() as u8 {
                                data.push(date_data.clone());
                                true
                            } else { false }
                        });
                    }
                    tx.send((Calendar::new(date, position, data, &config), index)).unwrap();
                }
            })
        }).collect();

        for handle in handles {
            handle.join().unwrap();
        }

        for _ in 0..max {
            let (calendar, index) = rx.recv().unwrap();
            *vec.get_mut(index).unwrap() = calendar;
        }

        self.calendars = vec;
    }

    fn draw_calendars(&self) -> Formatter {
        let threads = self.config.max_threads;
        let threads: usize = if self.calendars.len() > threads { threads } else { self.calendars.len() };
        let (tx, rx) = channel();
        let mutex = Arc::new(Mutex::new(self.calendars.clone()));
        let handles: Vec<_> = (0..threads).map(|_| {
            let mutex = Arc::clone(&mutex);
            let tx = tx.clone();
            thread::spawn(move || {
                loop {
                    let mut calendar: Calendar;
                    {// Introduce scope to remove lock fast
                        match mutex.lock().unwrap().pop() {
                            Some(value) => calendar = value,
                            None => break,
                        };
                    }
                    tx.send(calendar.draw_format()).unwrap();
                }
            })
        }).collect();

        // Wait for all threads to finish
        for handle in handles {
            handle.join().unwrap();
        }

        // Collect thread messages
        let mut format = Formatter::new();
        for _ in 0..self.calendars.len() {
            format += &rx.recv().unwrap();
        }
        
        self.draw_background() + &format
    }

    fn move_calendar(&mut self, index: &mut usize, direction: Direction) -> Formatter {
        let change;
        let mut format = Formatter::new();
        match direction {
            Direction::Up | Direction::Down => {
                let x = get_columns();
                if let Direction::Up = direction {
                    if *index == 0 || *index < x { return format; }
                } else if *index + x >= self.calendars.len() { return format; }
                change = x;
            },
            _ => {
                if let Direction::Left = direction {
                    if *index == 0 { return format; }
                } else if *index + 1 >= self.calendars.len() { return format; }
                change = 1;
            },
        }
        let calendar = self.calendars.get_mut(*index).unwrap();
        if self.config.unselect_change_calendar_cursor || self.config.change_calendar_reset_cursor {
            format += &calendar.unselect_button(&mut self.config);
        }
        if matches!(direction, Direction::Down) || matches!(direction, Direction::Right) { 
            *index += change; 
        } else { *index -= change; }
        if self.config.change_calendar_reset_cursor { calendar.cursor = 0; }
        let calendar = self.calendars.get_mut(*index).unwrap();
        format + &calendar.select_button(&mut self.config, calendar.cursor)
    }

    fn reset(&mut self, date: Date<Local>) {
        self.terminal.reset();
        self.bounds = position::get_boundaries();
        self.config = Config::get_config();
        self.save = Some(Save::new(self.config.save_file.clone()));
        self.calendars.clear();
        self.init(date);
    }
}

fn get_columns() -> usize {
    let mut position = Position::new_origin();
    let mut count:  usize = 0;
    loop {
        if !position.set_x(position.get_x() + 23) {
            break;
        } else { count += 1 } 
        
        if !position.set_x(position.get_x() + 1) {
            break;
        }
    }
    count
}

fn get_rows() -> usize {
    let mut position = Position::new_origin();
    let mut count: usize = 0;
    loop {
        if !position.set_y(position.get_y() + 13) {
            break;
        } else { count += 1; }
        
        if !position.set_y(position.get_y() + 1) {
            break;
        }
    }
    count
}